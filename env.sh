#!/bin/bash

export MONGO_URL='mongodb://localhost:27017/carebuddy'
export ROOT_URL='http://demo.carebuddy.co'
export MAIL_URL='smtp://localhost:25/'
export KADIRA_PROFILE_LOCALLY=1
export METEOR_SETTINGS='$(cat config/settings.json)'