if (Meteor.isServer) { 
    Meteor.methods({
        otp: function (userPhone) {
            check(userPhone, String);

            const num = Random._randomString(6, '1234567890');

            const aid = '633070';
            const pin = 'car@3';
            let message = 'Hello,%0aA warm welcome!!!%0aVerify your phone number by typing OTP - ' + num + '.%0aPing your Personal Health Assistant right away.%0aTake Care.';

            let url = 'http://luna.a2wi.co.in:7501/failsafe/HttpLink?aid=' + aid + '&pin=' + pin + '&mnumber=91' + userPhone + '&message=' + message + ' &signature=CAREBD';
            
            this.unblock();

            HTTP.call("POST", url, function (error, result) {
                if (!error) {
                    if(result.statusCode == 200) {
                        ServerSession.set(userPhone, num);
                    } else {
                        throw new Meteor.Error(result.statusCode, result.content); 
                    }
                } else {
                    return error;
                }
            });
        },
        text: function (object) {
            if (! this.userId) {
                throw new Meteor.Error('not-logged-in',
                    'Must be logged to create a chat.');
            }

            check(object, {
                phone: String,
                message: String
            });

            const aid = '633070';
            const pin = 'car@3';
            const userPhone = object.phone;
            const message = object.message;
            
            var sms = {
                userPhone: userPhone,
                message: message,
                createdAt: new Date(),
                status: {
                    content: 'Initialized'
                }
            };

            var smsId = SMS.insert(sms);

            let url = 'http://luna.a2wi.co.in:7501/failsafe/HttpLink?aid=' + aid + '&pin=' + pin + '&mnumber=' + userPhone + '&message=' + message + ' &signature=CAREBD';
            this.unblock();
            
            HTTP.call("POST", url, function (error, result) {
                if (error) {
                    SMS.update(smsId, {
                        $set: {
                            'status.content': '',
                            'status.error': error
                        } 
                    });
                    return false;
                } else {
                    SMS.update(smsId, {
                        $set: { 
                            'status.statusCode': result.statusCode, 
                            'status.content': result.content 
                        } 
                    });
                    if(result.statusCode == 200) {
                        return true;
                    } else {
                        return false;
                    }
                }
            });
        }
    });
}