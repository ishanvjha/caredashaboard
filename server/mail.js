Meteor.methods({
  sendEmail: function (to, from, subject, text) {
    check([to, from, subject, text], [String]);

    this.unblock();

    Email.send({
      to: to,
      from: from,
      subject: subject,
      text: text
    });
  },
  cbMail: function(actionType, data, date, time) {
    SSR.compileTemplate( 'appointmentEmail', Assets.getText( 'newAppointment.ng.html' ) );
    SSR.compileTemplate( 'newMemberEmail', Assets.getText( 'newMember.ng.html' ) );
    SSR.compileTemplate( 'newUserEmail', Assets.getText( 'newUser.ng.html' ) );
    SSR.compileTemplate( 'newOrder', Assets.getText( 'medicineOrder.ng.html' ) );
    SSR.compileTemplate( 'newMsg', Assets.getText( 'newMsg.ng.html' ) );

    var to = "app.carebuddy@gmail.com";
    var from = actionType + " <no-reply@carebuddy.co>";
    var template;
    var subject;
    var emailData;

    if(actionType == 'User Appointment') {
      template = 'appointmentEmail';
      subject = data.type.name + ' added by - ' + Meteor.user().profile.name + ' (' + Meteor.user().username + ')';
      emailData = {
        type: data.type.name,
        name: data.name,
        phone: data.phone,
        address: data.address,
        comments: data.comments,
        date: date,
        time: time,
        user: data.userId.profile.name + ' (' + data.userId.username + ')',
        createdBy : Meteor.user().name + ' (' + Meteor.user().username + ')',
        createdAt : new Date()
      };
    } else if(actionType == 'New Member') {
      template = 'newMemberEmail';
      subject = actionType + ' added by - ' + Meteor.user().profile.name + ' (' + Meteor.user().username + ')';
      emailData = {
        firstName: data.firstName,
        lastName: data.lastName,
        phone: data.phone,
        gender: data.gender,
        relationType: data.relation,
        relationOther: data.other,
        user: Meteor.user().profile.name + ' (' + Meteor.user().username + ')',
        createdAt : new Date()
      };
    } else if(actionType == 'New User Registration') {
      template = 'newUserEmail';
      subject = actionType + ' - ' + data.name + ' (' + data.username + ')';
      emailData = {
        name: data.name,
        phone: data.username,
        email: data.email,
        createdAt : new Date()
      };
    } else if(actionType == 'New Message') {
      template = 'newMsg';
      subject = actionType + ' from ' + Meteor.user().profile.name + ' (' + Meteor.user().username + ')';
      emailData = {
        msg: data,
        date: date,
        time: time
      };
    } else if(actionType == 'Medicine Order') {
      template = 'newOrder';
      subject = actionType + ' from ' + Meteor.user().profile.name + ' (' + Meteor.user().username + ')';
      emailData = {
        comments: data.comments,
        user: data.userId.profile.name,
        status: 'Placed order',
        file: data.downloadUrl,
        createdBy : Meteor.user().profile.fname + ' (' + Meteor.user().username + ')',
        date: date,
        time: time
      };
    }

    Email.send({
      to: to,
      from: from,
      subject: subject,
      html: SSR.render( template, emailData )
    });
  }
});