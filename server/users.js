Meteor.startup(function () {
	//process.env.MAIL_URL = 'smtp://ajaypalnitj:9k_68QUCdLU6EaaoZo9iEg@smtp.mandrillapp.com:587/';
	process.env.MAIL_URL = 'smtp://localhost:25/';

	Accounts.emailTemplates.siteName = 'CareBuddy';
	Accounts.emailTemplates.from = 'CareBuddy <care@carebuddy.co>';

	SSR.compileTemplate( 'enrollmentEmail', Assets.getText( 'enrollmentEmail.ng.html' ) );

	var user_root_path = 'http://app.carebuddy.co/#/';

	Accounts.urls.enrollAccount = function (token) {
		return user_root_path + 'enroll-account/' + token;
	};

	Accounts.emailTemplates.verifyEmail.subject = function(user) {
		return 'Confirm Your Email Address for Carebuddy';
	};
	Accounts.emailTemplates.enrollAccount.subject = function (user) {
		return "CareBuddy invites you";
	};

	Accounts.emailTemplates.verifyEmail.html = function(user, url) {
		return 'Thank you for registering.  Please click on the following link to verify your email address: \r\n' + url;
	};
	Accounts.emailTemplates.enrollAccount.html = function(user, url) {
		var emailData = {
			user: user,
			url: url
		};
		return SSR.render( 'enrollmentEmail', emailData );
	};

	Accounts.config({
		sendVerificationEmail: false,
		loginExpirationInDays: null
    	//restrictCreationByEmailDomain: 'carebuddy.co',
	});
});

Meteor.methods({
	newMember: function (member) {
		var memberId = Accounts.createUser(member);
		Roles.addUsersToRoles(memberId, 'user');
		return memberId;
	},
	newCarebuddy: function (member) {
		var memberId = Accounts.createUser(member);
		Roles.addUsersToRoles(memberId, 'carebuddy');
		return memberId;
	},
	userEnrollment: function (member) {
		var userId = Accounts.createUser(member);
		Roles.addUsersToRoles(userId, 'user');
		if(userId) {
			Accounts.sendEnrollmentEmail(userId);
		} else {
			console.log('Error!');
		}
		return userId;
	}
});