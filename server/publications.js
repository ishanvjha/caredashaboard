if (Meteor.isServer) {
	Meteor.publish('get-users', function() {
		if(this.userId) {
			var user = Meteor.users.findOne(this.userId);
			if ( user.roles == 'carebuddy' ) {
				return Meteor.users.find({ roles: 'user' }, { fields: { services: 0 } });
			} else {
				return Meteor.users.find({_id: user.id}, { fields: { profile: 1 } });
			}
		}
	});

	Meteor.publishComposite('chats', function (userId) {
		if (! this.userId) {
			return;
		}

		return {
			find: function () {
				return Chats.find({ userIds: userId });
			},
			children: [
			{
				find: function (chat) {
					return Messages.find({ chatId: chat._id });
				}
			}
			]
		}
	});

	Meteor.publish('members', function () {
		if (this.userId) {
			return Meteor.users.find({}, { fields: { 'profile': 1} });
		} else {
			this.ready();
		}
	});

	Meteor.publish('appointMem', function () {
		if (this.userId) {
			return Meteor.users.find({ "profile.userType": "user"}, { fields: { profile: 1 } });
		} else {
			this.ready();
		}
	});

	Meteor.publish('sms', function () {
		return SMS.find();
	});

	Meteor.publish('appointments', function (userId) {
		return Appointments.find({ user: userId });
	});

	Meteor.publish('uploads', function (userId) {
		return Uploads.find({ user: userId });
	});

	Meteor.publish('medicines', function ( userId ) {
		return Medicines.find({ user: userId });
	});
}