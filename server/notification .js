Meteor.methods({
	// serverNotification: function () {
	// 	var last = NotificationHistory.findOne({}, {sort: {addedAt: -1}});
	// 	var badge = 1
	// 	if (last != null) {
	// 		badge = last.badge + 1;
	// 	}

	// 	NotificationHistory.insert({
	// 		badge: badge,
	// 		addedAt: new Date()
	// 	}, function (error, result) {
	// 		if (!error) {
	// 			Push.send({
	// 				from: 'push',
	// 				title: 'Hello World',
	// 				text: 'This notification has been sent from the SERVER',
	// 				badge: badge,
	// 				payload: {
	// 					title: 'Hello World'
	// 				},
	// 				query: {}
	// 			});
	// 		}
	// 	});
	// },
	userNotification: function(title) {
		var last = NotificationHistory.findOne({}, {sort: {addedAt: -1}});
		var badge = 1
		if (last != null) {
			badge = last.badge + 1;
		}

		NotificationHistory.insert({
			badge: badge,
			addedAt: new Date()
		}, function (error, result) {
			if (!error) {
				Push.send({
					from: title.userId,
					title: title.title,
					text: title.text,
					badge: badge,
					sound: 'airhorn.caf',
					payload: {
						title: title.title
					},
					query: {
						userId: title.userId
					}
				});
			} else {
				toastr.error(error);
			}
		});
	},
	// browserNotification: function(title) {
	// 	if (!Meteor.isCordova) {
	// 		BrowserNotifications.sendNotification({
	// 			userId: title.userId,
	// 			title: title.title,
	// 			body: title.text
	// 		});
	// 	}
	// },
    removeHistory: function () {
      NotificationHistory.remove({}, function (error) {
         if (!error) {
            console.log("All history removed");
        }
    });
  }
});