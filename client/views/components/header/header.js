Template.header.onRendered(function() {
	//this.subscribe('chats');
});

Template.header.helpers({
	users: function() {
		return Meteor.users.find().fetch().map(function(user){
			console.log(user);
			return user.profile.name; 
		});
	},
	settings: function() {
		return {
			position: "top",
			limit: 5,
			rules: [
			{
				token: '@',
				collection: Meteor.users,
				field: "username"
			}
			]
		};
	}
	// lastestMsg: function() {
	// 	Chats.find().observe({
	// 		changed: function(chat) {
	// 			var user = Meteor.users.findOne(chat.lastMessage.userId, { fields: { profile: 1, username: 1, roles: 1 } });
	// 			if(user.roles == 'user') {
	// 				if(user.profile) {
	// 					if(user.profile.firstname) {
	// 						userName = user.profile.firstname + ' ' + user.profile.lastname;
	// 					} else if(user.profile.name) {
	// 						userName = user.profile.name;
	// 					}
	// 				} else {
	// 					userName = user.username;
	// 				}
	// 				var now = chat.lastMessage.timestamp;
	// 				var before = moment({ hour: 9 })._d;
	// 				var after = moment({ hour: 17 })._d;
	// 				if (moment(now).isBetween(before, after) == false) {
	// 					Meteor.call('newMessage', {
	// 						text: 'The availablity timing is 9 am to 5 pm, kindly leave your message. We will reply you as soon as our team is online. Thanks.',
	// 						type: 'text',
	// 						chatId: chat._id
	// 					}, function(error, response){
	// 						if (error) {
	// 							$.growl.error({ title: "Error", message: error.reason });
	// 						}
	// 					});
	// 				}
	// 				var msg;

	// 				if(chat.lastMessage.text) {
	// 					msg = chat.lastMessage.text;
	// 				} else if (chat.lastMessage.image) {
	// 					msg = "New Image recieved";
	// 				}

	// 				$.growl({ title: userName, message: msg });
	// 			}
	// 		}
	// 	});


		// const date = new Date().getDate(),
		// month = new Date().getMonth(),
		// year = new Date().getYear();

		// const dt = new Date(year, month, date);

		// return Chats.find({ "lastMessage.timestamp": { $gte: dt } }, { limit: 5 });
	//}
});