Template.sidebar.onRendered(function() {
	$(function () {
		if ($(window).width()<840) {
			if ($('body').hasClass('extended')==1) {
				$('body').removeClass('extended')
			}     
		}
		var sessionLayout = "";
		if (!sessionLayout && $(window).width()>1600) {
			$('.c-hamburger').addClass('is-active');
			$('body').addClass('extended');        
			$('.scrollbar').perfectScrollbar(); 
		};

		$( ".c-hamburger" ).click(function() {

			$( this ).toggleClass('is-active');
			$('body').toggleClass('extended');

			var hasClass = $('body').hasClass('extended');

			$.get('/api/change-layout?layout='+ (hasClass ? 'extended': 'collapsed'));

		});

		if ($('body').hasClass('extended')==1) {
			$( ".c-hamburger" ).addClass('is-active') ;
		};
		$( ".show-menu" ).click(function() {
			$( this ).parent().find('.sub-menu').toggleClass('visible');
		});

		$(window).resize(function  () {
			if ($(window).width()>1600) {
				$('.c-hamburger').addClass('is-active');
				$('body').addClass('extended');
				$('.scrollbar').perfectScrollbar();      
			};
			if ($(window).width()<1200) {
				$('.c-hamburger').removeClass('is-active');
				$('body').removeClass('extended');
			};  
			if ($('body').hasClass('extended')==1) {
				$('#scroll').addClass('ps-container');
				$('.scrollbar').perfectScrollbar();      
				$('.scrollbar').perfectScrollbar('update');
			}
			else if ($('body').hasClass('extended')==0) {
				$('#scroll').removeClass('ps-container');
			}               
		});      

		$( ".c-hamburger" ).click(function() {
			if ($('body').hasClass('extended')==1) {
				$('#scroll').addClass('ps-container');
				$('.scrollbar').perfectScrollbar();      
				$('.scrollbar').perfectScrollbar('update');
			} else if ($('body').hasClass('extended')==0) {
				$('#scroll').removeClass('ps-container');
			}
		});
	});
});

Template.sidebar.helpers({
	allUsers: function() {
		return Meteor.users.find({ roles: 'user' }, { fields: { userDetails: 1, status: 1, username: 1 } });
	}
});