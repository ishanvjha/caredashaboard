Template.register.events({
	'submit form': function( event, template ) {
		event.preventDefault();

		var email = $('[name=email]').val();
		var password = $('[name=password]').val();

		if(_.isEmpty(email)) {
			$.growl.error({ title: "Error", message: "Enter your email address to continue." });
		} else if(_.isEmpty(password)) {
			$.growl.error({ title: "Error", message: "Enter your password to continue." });
		} else {
			Meteor.call('newCarebuddy', {
				email     : email,
				password  : password,
			}, function(error, response){
				if (error) {
					$.growl.error({ title: "Error", message: error.reason });
				} else {
					$.growl.notice({ title: "Error", message: "User Registered, login to continue" });
					document.getElementById('register-form').reset();
					FlowRouter.go('/login');
				}
			});
		}
	}
});