Template.login.events({
	'submit form': function(event, template) {
		event.preventDefault();

		var email = $('input[name=email]').val();
		var password = $('input[name=password]').val();
		
		if(_.isEmpty(email)) {
			$.growl.error({ title: "Error", message: "Enter your email address to continue." });
		} else if(_.isEmpty(password)) {
			$.growl.error({ title: "Error", message: "Enter your password to continue." });
		} else {
			Meteor.loginWithPassword(email, password, function(error, response) {
				if (error) {
					$.growl.error({ title: "Error", message: error.reason });
				} else {
					$.growl.notice({ title: "Login Success", message: "Redirecting to your dashboard" });
				}
			});
			document.getElementById('login-form').reset();
		}
	}
});