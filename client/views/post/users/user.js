Template.user.onCreated( function() {
	this.currentTab = new ReactiveVar("profile");
});

Template.user.helpers({
	tab: function() {
		return Template.instance().currentTab.get();
	},
	tabData: function() {
		const tab = Template.instance().currentTab.get();
		const userId = FlowRouter.getParam('_id');
		const user = Meteor.users.findOne({ _id: userId });

		if( user && user.userDetails && user.userDetails.members ) {
			mems = user.userDetails.members;
		} else {
			mems = [];
		}
		
		const data = {
			"profile": user,
			"records": Uploads.find().fetch(),
			"medicine": Medicines.find().fetch(),
			"members": mems,
			"appointments": Appointments.find().fetch()
		};

		return data[ tab ];
	},
	userInfo: function() {
		return Meteor.users.findOne({ _id: FlowRouter.getParam('_id') }, { fields: { userDetails: 1, username: 1 } });
	},
	isReady: function(sub) {
		if(sub) {
			return FlowRouter.subsReady(sub);
		} else {
			return FlowRouter.subsReady();
		}
	},
	messages: function() {
		var chat = Chats.find().fetch();
		
		if (_.isEmpty(chat[0])) { 
			Meteor.call('newChat', FlowRouter.getParam('_id'), function(ncError, ncResult) {
				if (ncError) {
					$.growl.error({ title: "Error", message: ncError.reason });
				} else {
					Meteor.call('welcomeMessage', {
						text: "Hello there , thanks for using our app. If you need any help, just let me know. I'm available to answer any questions you may have.",
						type: 'text',
						chatId: ncResult
					});
				}						
			});
		} else {
			return Messages.find({ chatId: chat[0]._id }).fetch();
		}
	},
	equals: function(v1, v2) {
		if(v1 == v2){
			return true;
		} else {
			return false;
		}
	},
	paramId: function() {
		return FlowRouter.getParam('_id');
	}
});

Template.user.events({
	'click .mdl-tabs__tab-bar a': function( event, template ) {
		var currentTab = $( event.target ).closest( "a" );

		currentTab.addClass( "is-active" );
		$( ".mdl-tabs__tab-bar a" ).not( currentTab ).removeClass( "is-active" );

		template.currentTab.set( currentTab.data( "template" ) );
	},
	// 'click .app-tabs': function( event, template ) {
	// 	if ($( event.target ).hasClass('active')) {
	// 		$( event.target).parent().find('.app-table').stop().slideUp();
	// 		$( event.target).removeClass('active');
	// 	} else {
	// 		$('.app-tabs').removeClass('active');
	// 		$('.app-table').slideUp();
	// 		$( event.target).parent().find('.app-table').stop().slideToggle();
	// 		$( event.target).addClass('active');
	// 	}
	// },
	'click #message, keypress': function( event, template ) {
		if ((event.type === 'click') || (event.type === 'keypress' && event.which === 13) ) {
			event.preventDefault();
			var message = $('[name=message]').val();
			var chat = Chats.findOne();

			chatId = chat._id;
			if (_.isEmpty(message)) {
				return;
			}

			Meteor.call('newMessage', {
				text: message,
				type: 'text',
				chatId: chatId
			}, function(error, response){
				if (error) {
					$.growl.error({ title: "Error", message: error.reason });
				} else {
					Meteor.call("userNotification", {
						title: 'CareBuddy',
						text: message,
						userId: chat.userIds
					});
				}
				$('[name=message]').val('');
			});
		}
	}
});