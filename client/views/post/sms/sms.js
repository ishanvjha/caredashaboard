Template.sms.onRendered(function() {
	$('#sms-form').validate({
		rules: {	
			phone: {
				required: true
			},
			message: {
				required: true
			}
		}, 
		messages: {
			phone: {
				required: "Phone is required"
			}, 
			message: {
				required: "Message is required"
			}
		},
		submitHandler() {
			const phone =  $('[name="phone"]').val(),
			message = $('[name="message"]').val();

			Meteor.call('text', {
				phone: phone,
				message: message
			}, function(error, response){
				if (error) {
					$.growl.error({ title: "Error", message: error.reason });
				} else {
					$.growl.notice({ title: "Success", message: "SMS Sent" });
					document.getElementById('sms-form').reset();
				}
			});
		}
	});
});

Template.sms.helpers({
	sent: function() {
		return SMS.find().fetch();
	}
});

Template.sms.events({
	'submit form': function( event ) {
		event.preventDefault();
	},
	'click .collapse-card__heading':  function( event ) {
		event.preventDefault();
		$(event.currentTarget.parentNode).toggleClass('active');
	}
});