Template.recordValues.onRendered(function() {
	$('#record-values').validate({
		rules: {	
			time: {
				required: true
			},
			date: {
				required: true,
				date: true
			},
			values: {
				required: true
			},
			signs: {
				required: true
			},
			measurement: {
				required: true
			},
			valUser: {
				required: true
			}
		}, 
		messages: {
			time: {
				required: "Time is required."
			}, 
			date: {
				required: "Date is required.",
				date: "Not a valid date."
			},
			values: {
				required: "Enter the value."
			},
			signs: {
				required: "Signs is required."
			},
			measurement: {
				required: "Measurement is required."
			},
			valUser: {
				required: "User not selected."
			}, 
		},
		submitHandler() {
			const time = $('[name=time').val(),
			date = $('[name=date]').val(),
			values = $('[name=values').val(),
			signs = $('[name=signs]').val(),
			measurement = $('[name=measurement]').val(),
			comments = $('[name=comments').val();

			Uploads.insert({
				type: 'value',
				user: FlowRouter.getParam('_id'),
				time: time,
				date: date,
				values: values,
				signs: signs,
				measurement: measurement,
				comments: comments,
				createdAt : new Date()
			}, function( error, response ) {
				if(error) {
					$.growl.error({ title: "Error", message: error.reason });
				} else {
					$.growl.notice({ title: "Success", message: "Document uploaded successfully" });
					document.getElementById('record-values').reset();
					$('.collapse-card').removeClass('active');
				}				
			});
		}
	});
});

Template.recordValues.events({
	'submit form': function( event, template ) {
		event.preventDefault();
	}
});