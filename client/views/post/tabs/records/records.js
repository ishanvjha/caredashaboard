Template.records.helpers({
	equals: function(v1, v2) {
		if(v1 == v2){
			return true;
		} else {
			return false;
		}
	}
});

Template.records.events({
	'click .drop-head': function(event, template) {
		if ( $(event.currentTarget).hasClass('active')) {
			$(event.currentTarget.nextElementSibling).stop().slideUp(200);
			$(event.currentTarget).removeClass('active');
		} else {
			$('.drop-head').removeClass('active');
			$('.drop-body').slideUp(200);
			$(event.currentTarget.nextElementSibling).stop().slideToggle(200);
			$(event.currentTarget).addClass('active');
		}
	},
	'click .collapse-card__heading':  function( event ) {
		event.preventDefault();
		$(event.currentTarget.parentNode).toggleClass('active');
	}
});