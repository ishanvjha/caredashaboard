Template.recordDetails.onRendered(function() {
	$('#records-detail').validate({
		rules: {	
			imgRecordType: {
				required: true
			},
			imgDate: {
				required: true,
				date: true
			},
			imgRecord: {
				required: true
			}
		}, 
		messages: {
			imgRecordType: {
				required: "Select a record type"
			}, 
			imgDate: {
				required: "Date is required.",
				date: "Not a valid date."
			},
			imgRecord: {
				required: "Image not selected"
			}
		},
		submitHandler() {
			const comments = $('[name=imgComments').val(),
			record = $('[name=imgRecordType]').val(),
			date = $('[name=imgDate').val();
			
			var file = document.getElementById('myFileInput').files[0];
			
			var metaContext = { albumId: FlowRouter.getParam('_id'), folder: 'cb_records' }
			var uploader = new Slingshot.Upload("myFileUploads", metaContext);

			uploader.send(file, function (error, downloadUrl) {
				if (error) {
					console.log(error);
				} else {
					Uploads.insert({
						url: downloadUrl,
						type: 'image',
						user: FlowRouter.getParam('_id'),
						date: date,
						record: record,
						comments: comments,
						createdAt : new Date()
					}, function(eor,ros) {
						if(eor) {
							$.growl.error({ title: "Error", message: eor.reason });
						} else {
							$.growl.notice({ title: "Success", message: 'Document uploaded successfully' });
							$('.collapse-card').removeClass('active');
						}
					});
					document.getElementById('records-detail').reset();
				}
			});			
		}
	});
});

Template.recordDetails.events({
	'submit form': function( event ) {
		event.preventDefault();
	}
});