Template.orderDetails.onRendered(function() {
	this.subscribe('medicines');
});

Template.orderDetails.helpers({
	orderDetail: function() {
		return Medicines.find({ _id: Session.get('orderId') }).fetch();
	}
});

Template.orderDetails.events({
	'click .responsive-img': function(event, template) {
		$('.responsive-img').magnificPopup({
			items: {
				src: event.currentTarget.currentSrc,
				type: 'image'
			}
		}).magnificPopup('open');
	}
});