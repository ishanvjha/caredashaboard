// Template.medicine.onRendered(function() {
// 	var self = this;
// 	const userId = FlowRouter.getParam('_id');
// 	self.autorun(function() {
// 		self.subscribe('medicines', userId);  
// 	});
// });

// Template.medicine.helpers({
// 	medis: function() {
// 		return Medicines.find().fetch();
// 	}
// });

Template.medicine.events({
	'click #order, keypress': function( event, template ) {
		if ((event.type === 'click') || (event.type === 'keypress' && event.which === 13) ) {
			event.preventDefault();
		}
	},
	'click .refresh': function( event, template ) {
		event.preventDefault();
		Session.set('orderId', '');
		Session.set('orderId', $(event.currentTarget).attr('order-id'));
		const inst = $('[data-remodal-id=orderDetails]').remodal();
		inst.open();
	}
});