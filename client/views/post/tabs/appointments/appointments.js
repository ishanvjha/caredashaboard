// Template.appointments.onCreated( function() {
// 	this.subscribe('appointments');
// });

Template.appointments.onRendered( function() {
	$('#appoint-form').validate({
		rules: {	
			type: {
				required: true
			},
			name: {
				required: true
			}
		}, 
		messages: {
			type: {
				required: "Appointment Type is required"
			}, 
			name: {
				required: "Name is required"
			}
		},
		submitHandler() {
			const type = $('[name=type]').val(),
			name =  $('[name=name]').val(),
			number =  $('[name=number]').val(),
			date =  $('[name=date]').val(),
			time =  $('[name=time]').val(),
			address =  $('[name=address]').val(),
			comments =  $('[name=comments]').val(),
			userId = FlowRouter.getParam('_id');

			Appointments.insert({
				appointmentType: type,
				appointmentName: name,
				appointmentPhone: number,
				appointmentAddress: address,
				appointmentComments: comments,
				appointmentDate: date,
				appointmentTime: time,
				status: 'created',
				user: userId,
				createdAt : new Date(),
			}, function(error, response) {
				if(error) {
					$.growl.error({ title: "Error", message: error.reason });
				} else {
					$.growl.notice({ title: "Success", message: "Appointment Added" });
					document.getElementById('appoint-form').reset();
				}
			});
		}
	});
});

// Template.appointments.helpers({
// 	appointments: function() {
// 		const userId = FlowRouter.getParam('_id');
// 		return Appointments.find({ user: userId }).fetch();
// 	}
// });

Template.appointments.events({
	'submit form': function( event ) {
		event.preventDefault();
	},
	'click .collapse-card__heading':  function( event ) {
		event.preventDefault();
		$(event.currentTarget.parentNode).toggleClass('active');
	}
});