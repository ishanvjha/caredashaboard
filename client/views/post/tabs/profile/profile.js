Template.profile.onRendered(function() {
	Tracker.autorun(function(){
		const userId = FlowRouter.getParam('_id');
		const user = Meteor.users.findOne({ _id: userId });

		if(user && user.profile) {
			$('[name=name]').val(user.profile.name);
			$('[name=dob]').val(user.profile.dob);
			$('[name=blood]').val(user.profile.blood);
			$('[name=allergies]').val(user.profile.allergies);
		} else if(user && user.userDetails && user.userDetails.profile) {
			$('[name=name]').val(user.userDetails.profile.name);
			$('[name=dob]').val(user.userDetails.profile.dob);
			$('[name=blood]').val(user.userDetails.profile.blood);
			$('[name=allergies]').val(user.userDetails.profile.allergies);
		}
	});
	
	$('#profile-form').validate({
		rules: {	
			name: {
				required: true
			},
			dob: {
				required: true,
				date: true
			}
		}, 
		messages: {
			name: {
				required: "Name is required."
			},
			dob: {
				required: "Date of birth is required.",
				date: "Valid date is required."
			}
		},
		submitHandler() {
			let profile = {
				name:  $('[name="name"]').val(),
				dob: $('[name="dob"]').val(),
				blood: $('[name="blood"]').val(),
				allergies: $('[name="allergies"]').val()
			};

			Meteor.users.update( FlowRouter.getParam('_id'), { $set: { 'userDetails.profile': profile } });
			
			$.growl.notice({ title: "Success", message: 'Profile updated.' });
		}
	});
});

Template.profile.events({
	'submit form': function(event) {
		event.preventDefault();
	}
});