Meteor.startup(function() {
	Meteor.typeahead.inject();

	Meteor.users.find({ "status.online": true }, { fields: { userDetails: 1, phone: 1, profile: 1, username: 1, roles: 1 } }).observe({
		added: function(user) {
			if(user.profile) {
				if(user.profile.firstname) {
					userName = user.profile.firstname + ' ' + user.profile.lastname;
				} else if(user.profile.name) {
					userName = user.profile.name;
				}
			} else if(user.userDetails && user.userDetails.profile && user.userDetails.profile.name) {
				userName = user.userDetails.profile.name;
			} else {
				userName = user.username;
			}

			$.growl.notice({ title: userName, message: "has appeared online" });
		},
		removed: function(user) {
			if(user.profile) {
				if(user.profile.firstname) {
					userName = user.profile.firstname + ' ' + user.profile.lastname;
				} else if(user.profile.name) {
					userName = user.profile.name;
				}
			} else if(user.userDetails && user.userDetails.profile && user.userDetails.profile.name) {
				userName = user.userDetails.profile.name;
			} else {
				userName = user.username;
			}

			$.growl.warning({ title: userName, message: "has gone offline" });
		}
	});
});