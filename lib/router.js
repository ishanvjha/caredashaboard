if (Meteor.isClient) {
	BlazeLayout.setRoot('body');

	AccountsTemplates.configureRoute('signIn', {
		layoutType: 'blaze',
		name: 'Login',
		path: '/login',
		template: 'login',
		layoutTemplate: 'appLayout',
		layoutRegions: {
			top: 'siteTitle',
			template: "login"
		},
		contentRegion: 'main'
	});

	AccountsTemplates.knownRoutes.push('logout');

	Accounts.onLogin(function() {
		var path = FlowRouter.current().path;

		if(path === "/" || "/login" || "/register"){
			FlowRouter.go("/dashboard/home");
		}
	});

	FlowRouter.subscriptions = function() {
		this.register('users', Meteor.subscribe('get-users'));
	};
}

FlowRouter.route('/', {
	name: 'Welcome',
	action: function() {
		BlazeLayout.render( "appLayout", { template: "welcome" });
	},
	fastRender: true
});

FlowRouter.route('/register', {
	name: 'Register',
	action: function() {
		BlazeLayout.render( "appLayout", { template: "register" });
	},
	fastRender: true
});

FlowRouter.route('/login', {
	name: 'Login',
	action: function() {
		BlazeLayout.render( "appLayout", { template: "login" });
	},
	fastRender: true
});

var dashboard = FlowRouter.group({
  prefix: '/dashboard',
  name: 'dashboard'
});

dashboard.route('/home', {
	name: 'Home',
	triggersEnter: [AccountsTemplates.ensureSignedIn],
	action: function() {
		BlazeLayout.render( "dashLayout", { top: "header", area: "home" });
	},
	fastRender: true
});

dashboard.route('/user/:_id', {
	name: 'User',
	triggersEnter: [AccountsTemplates.ensureSignedIn],
	subscriptions: function(params) {
		this.register('chat', Meteor.subscribe('chats', params._id));
		this.register('upload', Meteor.subscribe('uploads', params._id));
		this.register('medicine', Meteor.subscribe('medicines', params._id));
		this.register('appointment', Meteor.subscribe('appointments', params._id));
	},
    action: function() {
        BlazeLayout.render( "dashLayout", { top: "header", area: "user" });
    },
	fastRender: true
});

dashboard.route('/profile', {
	name: 'Profile',
	triggersEnter: [AccountsTemplates.ensureSignedIn],
	action: function() {
		BlazeLayout.render( "dashLayout", { top: "header", area: "cbProfile" });
	},
	fastRender: true
});

dashboard.route('/sms', {
	name: 'SMS',
	triggersEnter: [AccountsTemplates.ensureSignedIn],
	subscriptions: function(params, queryParams) {
        this.register('sms', Meteor.subscribe('sms'));
    },
	action: function() {
		BlazeLayout.render( "dashLayout", { top: "header", area: "sms" });
	},
	fastRender: true
});

dashboard.route('/email', {
	name: 'Email',
	triggersEnter: [AccountsTemplates.ensureSignedIn],
	action: function() {
		BlazeLayout.render( "dashLayout", { top: "header", area: "email" });
	},
	fastRender: true
});

FlowRouter.route('/logout', {
	action: AccountsTemplates.logout
});