NotificationHistory = new Mongo.Collection("notification_history");

Push.allow({
	send: function(userId, notification) {
		return true;
	}
});

Meteor.startup(function () {
	if (Meteor.isCordova) {
		window.alert = navigator.notification.alert;
	}

	Push.addListener('message', function(notification) {
		console.log(JSON.stringify(notification))

		function alertDismissed() {
			NotificationHistory.update({_id: notification.payload.historyId}, {
				$set: {
					"recievedAt": new Date()
				}
			});
		}
		alert(notification.message, alertDismissed, notification.payload.title, "Ok");
	});
})