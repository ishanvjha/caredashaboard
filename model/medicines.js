Medicines = new Mongo.Collection('medicines');

Medicines.allow({
  'insert': function(doc) {
    return doc;
  },
  'update': function(userId, doc, fields, modifier) {
    return userId === doc.userId;
  }
});